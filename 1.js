// map
const mapped = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].map(item => item + 1);
console.log(mapped);

// map を reduce で書き直す
const mapped2 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].reduce((acc, item) => {
  acc.push(item + 1);
  return acc;
}, []);
console.log(mapped2);

// map の引数を reduce の外へ
const mapReducer = f => (acc, item) => {
  acc.push(f(item));
  return acc;
};
const mapped3 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].reduce(
  mapReducer(item => item + 1),
  []
);
console.log(mapped3);

// reduce 処理を外へ
const mapping = f => reducing => (acc, item) => reducing(acc, f(item));
const mappingFunction = item => item + 1;
const reducingFunction = (acc, item) => {
  acc.push(item);
  return acc;
};
const mapped4 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].reduce(
  mapping(mappingFunction)(reducingFunction),
  []
);
console.log(mapped4);

// sample
const mappingSample = mapping(item => item + 1)((acc, item) => {
  acc.push(item);
  return acc;
})([], 1);
console.log(mappingSample);

// filter
const filtered = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].filter(item => item % 2 === 0);
console.log(filtered);

// filter を reduce で書き直す
const filtered2 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].reduce((acc, item) => {
  if (item % 2 === 0) {
    acc.push(item);
  }
  return acc;
}, []);
console.log(filtered2);

// filter の引数を reduce の外へ
const filterReducer = f => (acc, item) => {
  if (f(item)) {
    acc.push(item);
  }
  return acc;
};
const filtered3 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].reduce(
  filterReducer(item => item % 2 === 0),
  []
);
console.log(filtered3);

// reduce 処理を外へ
const filtering = f => reducing => (acc, item) =>
  f(item) ? reducing(acc, item) : acc;
const filterFunction = item => item % 2 === 0;
// const reducingFunction = (acc, item) => {
//   acc.push(item);
//   return acc;
// };
const filtered4 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].reduce(
  filtering(filterFunction)(reducingFunction),
  []
);
console.log(filtered4);

// sample
const filteringSample = filtering(item => item % 2 === 0)((acc, item) => {
  acc.push(item);
  return acc;
})([2, 4], 5);
console.log(filteringSample);

// map + filter
const mappedAndFiltered = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
  .map(item => item + 1)
  .filter(item => item % 2 === 0)
  .map(item => item * item)
  .filter(item => item % 8 === 0);
console.log(mappedAndFiltered);

// map + filter を書き換える
const mappedAndFiltered2 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].reduce(
  mapping(item => item + 1)(
    filtering(item => item % 2 === 0)(
      mapping(item => item * item)(
        filtering(item => item % 8 === 0)((acc, item) => {
          acc.push(item);
          return acc;
        })
      )
    )
  ),
  []
);
console.log(mappedAndFiltered2);
